import pkg from './package'

export default {
  mode: 'universal',
  router: {
    base: '/sucesso/'
  },
  /*
   ** Headers of the page
   */
  head: {
    htmlAttrs: {
      lang: 'pt-BR'
    },
    title: 'Sucesso | Velti Tecnologia',
    meta: [],
    link: [
      { rel: 'canonical', href: 'https://www.velti.com.br/sucesso/' },
      { rel: 'dns-prefetch', href: 'https://cdn-ca.jivosite.com' },
      { rel: 'dns-prefetch', href: 'https://files.jivosite.com' },
      { rel: 'dns-prefetch', href: 'https://code.jivosite.com' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'mask-icon', href: '/safari-pinned-tab.svg', color: '#e01d63' },
      { rel: 'shortcut icon', href: '/favicon.ico' },
      { rel: 'msapplication-TileColor', content: '#f7f7f7' },
      { rel: 'msapplication-config', content: '/browserconfig.xml' },
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#e01d63' },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [{src: '~/plugins/slider', ssr: false}],

  /*
   ** Nuxt.js modules
   */
  modules: ['nuxt-compress', '@nuxtjs/axios', '@nuxtjs/robots', '@nuxtjs/google-analytics', 'nuxt-facebook-pixel-module', '@nuxtjs/pwa'],

  googleAnalytics: {
    id: 'UA-120807099-1',
    dev: true,
    checkDuplicatedScript: true,
    autoTracking: {
      page: true,
      screenview: true
    },
    debug: {
      enabled: false
    }
  },

  facebook: {
    /* module options */
    track: 'PageView',
    pixelId: '2288825668057177',
    disabled: false
  },

  workbox: {
    offlineAnalytics: true,
    cleanupOutdatedCaches: true,
    cacheOptions:  {
      cacheId: 'VeltiSucesso',
      directoryIndex: '/',
      revision: undefined
    },
    offlineStrategy: 'NetworkFirst'
  },

  meta: {
    name: 'Sucesso | Velti Tecnologia',
    author: 'murden_dev',
    description: 'Sua compra foi concluida com sucesso!',
    theme_color: '#e01d63',
    lang: 'pt-BR'
  },

  manifest: {
    lang:"pt-BR",
    name:"Sucesso | Velti Tecnologia",
    short_name:"Sucesso",
    description:"Sua compra foi concluida com sucesso!",
    display: 'minimal-ui',
    start_url:"/sucesso/",
    background_color: '#f7f7f7'
  },


  robots: {
      UserAgent: '*',
      Allow: '',
      Disallow: '/sucesso'
    },

  /*
   ** Build configuration
   */
  build: {
    extend(config, ctx) {
    }
  }
}
